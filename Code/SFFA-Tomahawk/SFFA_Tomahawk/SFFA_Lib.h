/*--------------------------------------------------------------------
  This code is part of the SFFA Tomahawk.
  --------------------------------------------------------------------*/

/*!
 * \file SFFA_Lib.h
 * \This code makes the hardware and timing functions easy to use in the main code.
 * \author Willem Meijer, Bart van Deelen
 * \date 12-11-2019
 * \version 1.1
 * 
 * Version|Date        |Note
 * -------|------------|----
 * 1.1    | 12-11-2019 | added timing functionalities
 * 1.0    | 1-11-2019  | Initial version 
 * 
 */

#ifndef _SFFA_LIB_H_
#define _SFFA_LIB_H_

#include "SFFA_Hardware.h"

class Sensor
{
private:
    int _DataLog[145];           ///< Value log for the avg calculation. The array if 145 long because the max nr values we need to store is 144.
    uint8_t _Index = 0;         ///< start value of the array. 
    uint8_t _pin;               ///< Hardware pin to which the potentiometer is connected.
    int16_t _aRange;            ///< Minimum value that the potentiiometer will give
    int16_t _bRange;            ///< Maximum valie that the potentiometer will give.

public:
  /// \brief constructor
  /// \pre requires a analog input pin to which the potentiometer is connected
  /// The potentiometer shall apply 0 to VCC to the anlog input.
  /// \param hardwarePin Arduino pin to which the potentiometeris connected
  /// \param minimumValue of the range within the potentiometer will generate values. Default is 0
  /// \param maximumValue of the range within the potentiometer will generate values. Default is 1023.
  Sensor(uint8_t hardwarePin, int minimumValue = 0, int maximumValue = 1023);

  /// \brief Default destructor
  ~Sensor();

  /// \brief read value from potentiometer
  /// This function will transpose raw analog value form 0 to 1023 in to the range
  /// specified by the minimum and maximumvalue given at creation of the object.
  /// Then the value is saved in de DataLog[] so the avg can be calculated.
    float ReadValue();

  /// \brief calculate avarage
  /// This function will take an avarage of the measured values.
  /// \return float transposed value from potentiometer in to the sepcified range.
    float GetAvg();
    float GetMax();
    float GetDelta(long pollTime);
};

class Timing
{
  private: 

  public:
    long SendTimeNormal, sendTimePanic, SendTimeHeartbeat, SendTimeBatterylevel, SendTimeGPS;
    long PollTimeNormal, PollTimePanic, PollTimeHeartbeat;

    void SetToDefault();

    void SetToTest();
};

#endif
