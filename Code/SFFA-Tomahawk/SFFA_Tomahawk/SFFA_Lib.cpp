/*--------------------------------------------------------------------
  This code is part of the SFFA Tomahawk.
  --------------------------------------------------------------------*/

/*!
 * \file SFFA_Lib.cpp
 * \author Willem Meijer, Bart van Deelen
 */

#include "SFFA_Lib.h"

// Functions for class: iotShieldPotmeter
Sensor::Sensor(uint8_t hardwarePin, int minimumValue, int maximumValue)
  :_pin(hardwarePin)
  ,_aRange(maximumValue)
  ,_bRange(minimumValue)
{  
}

Sensor::~Sensor(){};

///Function to get the current value of the corresponding sensor
///The value is stored at the end of an logging array using #_Index
float Sensor::ReadValue()
{
  int rawValue    = analogRead(_pin);
  float mappedValue = map(rawValue, 0, 1023, _aRange, _bRange);
  _DataLog[_Index] = mappedValue;
  _Index++;
  return mappedValue;
}

///Function to get the average over all measurements since the last function call.
///The Logging array is cleared by clearing #_Index
float Sensor::GetAvg()
{
  float maxIndex = _Index;
  long sum = 0; 
  for(int i = 0; i < maxIndex; ++i)
    {
        sum += _DataLog[i];
        _DataLog[i] = 0;
    }
  _Index = 0;
  return sum/maxIndex;
}

///Function to get the maximum measurement value from the current Logging array.
float Sensor::GetMax()
{
  float max = -20;
  for(int i = 0; i < _Index; ++i)
    {
        if(_DataLog[i] > max)
          max = _DataLog[i];
    }
  return max;
}

///Function to get the difference between the current and previous measurement. 
///When the Logging array is empty, 0 will be returned.
float Sensor::GetDelta(long pollTime)
{
  if(_Index > 0)
    return ((_DataLog[_Index] - _DataLog[_Index - 1]) / pollTime) * 3600; ///Increase / decrease of sensorvalue in one hour
  else
    return 0;
}

///Function that will set all times to default.
///Used when not in test mode
void Timing::SetToDefault()
{
  SendTimeNormal = 3600;
  sendTimePanic = 30;
  SendTimeHeartbeat = 86400;
  SendTimeBatterylevel = 86400;
  SendTimeGPS = 2592000;

  PollTimeNormal = 60;
  PollTimePanic = 10;
  PollTimeHeartbeat = 600;
}

///Function that will set all times very low, to make testing easier.
///The function is called when entering test mode
void Timing::SetToTest()
{
  SendTimeNormal = 30;
  sendTimePanic = 30;
  SendTimeHeartbeat = 30;
  SendTimeBatterylevel = 30;
  SendTimeGPS = 30;

  PollTimeNormal = 10;
  PollTimePanic = 10;
  PollTimeHeartbeat = 10;
}
