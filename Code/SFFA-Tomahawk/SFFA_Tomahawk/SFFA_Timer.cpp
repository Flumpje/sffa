/*--------------------------------------------------------------------
  This code is part of the SFFA Tomahawk.

  This file is this code is derived from the HAN IoT shield library.
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*! 
 \file SFFA_Timer.cpp
 \author Remko Welling (PE1MEW)
*/

#include "SFFA_Timer.h"

#include "Arduino.h"

PE1MEW_Timer::PE1MEW_Timer():
  _msEndTime(0),
  _active(false)
{
}

// default destructor
PE1MEW_Timer::~PE1MEW_Timer()
{
}

bool PE1MEW_Timer::setExpiry(uint32_t msTime)
{
  // test if timer is running
  // If timer is running return false
  //    else returen true and set timer

  if(_active)
  {
    return false;
  }
  else
  {
    _msEndTime = millis() + msTime;
    _active = true;
    return true;
  }
}

  
bool PE1MEW_Timer::getExpired()
{
  if(millis() < _msEndTime)
  {
    return false;
  }
  else
  {
    _active = false;
    return true;
  }
}
