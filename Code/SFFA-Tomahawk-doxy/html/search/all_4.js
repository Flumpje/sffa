var searchData=
[
  ['err_5fcheck_5fconfiguration',['ERR_CHECK_CONFIGURATION',['../a00074.html#a8487bdf047b4233a6da3a15f6e87f4dc',1,'SFFA_TTN.h']]],
  ['err_5finvalid_5ffp',['ERR_INVALID_FP',['../a00074.html#ad1cccd0dd36debd5052638697527c8d9',1,'SFFA_TTN.h']]],
  ['err_5finvalid_5fsf',['ERR_INVALID_SF',['../a00074.html#a1b94f5909cdd8a239d89b8c5d8e276b5',1,'SFFA_TTN.h']]],
  ['err_5fjoin_5ffailed',['ERR_JOIN_FAILED',['../a00074.html#a67c02dc1d3b8a6690a2b413a615b2ef2',1,'SFFA_TTN.h']]],
  ['err_5fjoin_5fnot_5faccepted',['ERR_JOIN_NOT_ACCEPTED',['../a00074.html#a9880ab1bb4a04d880109e86067e5a9b8',1,'SFFA_TTN.h']]],
  ['err_5fkey_5flength',['ERR_KEY_LENGTH',['../a00074.html#a274c734e792ee029e0e9a3b0ecb1fe50',1,'SFFA_TTN.h']]],
  ['err_5fmessage',['ERR_MESSAGE',['../a00074.html#a4b3ab7702ac78455ee64be8a0667053c',1,'SFFA_TTN.h']]],
  ['err_5fpersonalize_5fnot_5faccepted',['ERR_PERSONALIZE_NOT_ACCEPTED',['../a00074.html#a38de18dbf253dfc23b46616a1837b762',1,'SFFA_TTN.h']]],
  ['err_5fresponse_5fis_5fnot_5fok',['ERR_RESPONSE_IS_NOT_OK',['../a00074.html#aaf260ad248abd64985cd506e9a7609bb',1,'SFFA_TTN.h']]],
  ['err_5fsend_5fcommand_5ffailed',['ERR_SEND_COMMAND_FAILED',['../a00074.html#a8e85ac1d90d63b62ef4ba500748dd81c',1,'SFFA_TTN.h']]],
  ['err_5funexpected_5fresponse',['ERR_UNEXPECTED_RESPONSE',['../a00074.html#a46ecac51e3be1684baf9f3e2d7a6d4fe',1,'SFFA_TTN.h']]],
  ['examples_2edox',['examples.dox',['../a00005.html',1,'']]]
];
