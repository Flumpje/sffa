@startuml
Start
:Start Timer;
repeat
:Reset Timer;
	if (30 seconds elapsed?) then (Yes)
	:Poll Sensors:\n Temperature\n Co;
	:Prepare Payload:\n Temp Average\n Temp Max\n Co Average\n CO Max\n Battery Level;
	:Send Data;
	:Calculate Derivatives,\n            Limits;
	else (No)
	stop
	endif
	
repeat while (Calculated values within range?) is (Yes)
->No;
:State_Change;

@enduml
