@startuml
Sensor -> Payload : Temp Average
Sensor -> Payload : Temp Maximum
Sensor -> Payload : CO average
Sensor --> Payload : CO Max
Sensor --> Payload : Battery Level

Payload -> TTN : Collected Data

@enduml
